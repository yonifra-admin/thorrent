package com.cryptocodes.thorrent2;

/**
 * Created by jonathanf on 12/11/2014.
 */
public enum Resolution {
    DVD,
    Cam,
    HDReady,
    FullHD,
    FourK,
    TwoK,
    ThreeD,
    Bluray,
    Telesync,
    NA
}
