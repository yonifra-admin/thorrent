package com.cryptocodes.thorrent2;

public enum Category {
    TV,
    MOVIE,
    MUSIC,
    BOOK,
    APPLICATION,
    GAME,
    NONE
}
